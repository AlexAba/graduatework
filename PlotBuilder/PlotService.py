from PlotBuilder import PlotBuilder
import numpy as np
import os
import sys


def main():

    params = sys.argv[1:]
    step = 1
    shouldShow = True

    i = 0
    end = len(params)
    while i < end:
        if(params[i] == "-step"):
            step = int(params[i + 1])
            i += 1
        elif(params[i] == "-show"):
            shouldShow = bool(int(params[i + 1]))
            i += 1
        i += 1

    path = os.getcwd() + "/"
    resPath = path + "Results/"
    programPath = path + "GraduateWork.exe "
    potentialName = "Potentials"
    ionConcentrationName = "IonConcentrations"
    elConcentrationName = "ElectronConcentrations"
    extention = ".txt"

    os.system(programPath + ' '.join(sys.argv[1:])) 

    PlotBuilder.FromFile(resPath + potentialName + extention, potentialName, "l", "fi", resPath, step).Build(shouldShow)
    PlotBuilder.FromFile(resPath + elConcentrationName + extention, elConcentrationName, "l", "ne", resPath, step).Build(shouldShow)
    PlotBuilder.FromFile(resPath + ionConcentrationName + extention, ionConcentrationName, "l", "ni", resPath, step).Build(shouldShow)


main()
