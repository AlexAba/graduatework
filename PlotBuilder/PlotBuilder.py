from matplotlib import pyplot as plt
import numpy as np

class PlotBuilder:
    def __init__(self, name = 'Plot', labelX = 'x', labelY = 'y', saveDirectory = "Plots"):
        self.name = name
        self.labelX = labelX
        self.labelY = labelY
        self.plots = []
        self.saveDirectory = saveDirectory

    def AddPlot(self, x, y, label):
        self.plots.append((x, y, label))

    def Build(self, shouldShow):
        for plot in self.plots:
            x, y, label = plot
            plt.plot(x, y, label=label)

        plt.title(self.name)
        plt.xlabel(self.labelX)
        plt.ylabel(self.labelY)
        # plt.legend()
        plt.savefig(self.saveDirectory + "/" + self.name)
        if(shouldShow):
            plt.show()

    @staticmethod
    def FromFile(dir, name = "Plot", labelX = "x", labelY = "y", saveDir = "Plots", step = 1):
        plotB = PlotBuilder(name, labelX, labelY, saveDir)
        array = np.genfromtxt(dir)
        
        i = 1
        end = len(array)

        while i < end:
            plotB.AddPlot(array[0], array[i], i)
            tempI = i
            i += step
            #   Always add last plot. 
            if(i > end - 1 and tempI < end - 1):
                i = end - 1
        return plotB