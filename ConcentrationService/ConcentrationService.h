#pragma once

#include <vector>

#include "../Coefficients/Coefficient.h"

using namespace std;

class ConcentrationService : public Coefficient
{
private:
    Coefficient &alpha;
    Coefficient &beta;

public:
    ConcentrationService(int size, Coefficient &alpha, Coefficient &beta, double lastConcentration);
    virtual void CalculateCoefficients() override;
};