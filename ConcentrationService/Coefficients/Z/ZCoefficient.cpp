#include "ZCoefficient.h"

#include <iostream>
using namespace std;

using namespace Concentration;

ZCoefficient::ZCoefficient(int size, int s, const Coefficient &p, const Coefficient &d, const Coefficient &m)
    : Coefficient(size), potential(p), diffusion(d), mobility(m)
{
    this->s = s;
}

void ZCoefficient::CalculateCoefficients()
{
    int size = coefficient.size() - 1;

    for (int i = 0; i < size; ++i)
    {
        coefficient[i] = -s * (mobility[i] / diffusion[i]) * (potential[i + 1] - potential[i]);
    }
}