#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Concentration
{
    class ZCoefficient : public Coefficient
    {
    private:
        int s;

        const Coefficient &potential;
        const Coefficient &diffusion;
        const Coefficient &mobility;

    public:
        ZCoefficient(int size, int s, const Coefficient &potential, const Coefficient &diffusion, const Coefficient &mobility);
        virtual void CalculateCoefficients() override;
    };
} // namespace Concentration