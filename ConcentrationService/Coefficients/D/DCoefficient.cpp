#include "DCoefficient.h"

using namespace Concentration;

DCoefficient::DCoefficient(int size, double deltaTime, const Coefficient &pC) 
: Coefficient(size), prevConcentration(pC)
{
    this->deltaTime = deltaTime;
}

void DCoefficient::CalculateCoefficients()
{
    int size = GetSize();
    for(int i = 0; i < size; ++i)
    {
        coefficient[i] = prevConcentration[i] / deltaTime;
    }
}