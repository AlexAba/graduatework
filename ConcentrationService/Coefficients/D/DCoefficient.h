#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Concentration
{
    class DCoefficient : public Coefficient
    {
    private:
        double deltaTime;
        const Coefficient &prevConcentration;

    public:
        DCoefficient(int size, double deltTime, const Coefficient &prevConcentration);
        virtual void CalculateCoefficients() override;
    };
} // namespace Concentration