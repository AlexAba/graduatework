#include <cmath>
#include "ACoefficient.h"

using namespace Concentration;

ACoefficient::ACoefficient(int size, double deltaStep, const Coefficient &d, const Coefficient &_Z)
    : Coefficient(size), diffusion(d), Z(_Z)
{
    this->deltaStep = deltaStep;
}

void ACoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size; ++i)
    {
        double z = Z[i];
        double zPart = 1;

        if (z != 0)
        {
            zPart = z / (exp(z) - 1);
        }

        coefficient[i] = -diffusion[i + 1] / (deltaStep * deltaStep) * zPart;
    }
}