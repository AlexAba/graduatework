#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Concentration
{
    class ACoefficient : public Coefficient
    {
    private:
        double deltaStep;

        const Coefficient &diffusion;
        const Coefficient &Z;

    public:
        ACoefficient(int size, double deltaStep, const Coefficient &diffusion,const  Coefficient &Z);
        virtual void CalculateCoefficients() override;
    };
} // namespace Concentration