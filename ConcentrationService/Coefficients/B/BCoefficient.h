#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Concentration
{
    class BCoefficient : public Coefficient
    {
    private:
        double deltaStep;
        double deltaTime;

        const Coefficient &diffusion;
        const Coefficient &Z;

    public:
        BCoefficient(int size, double deltaStep, double deltaTime, const Coefficient &diffusion, const Coefficient &Z);
        virtual void CalculateCoefficients() override;
    };
} // namespace Concentration