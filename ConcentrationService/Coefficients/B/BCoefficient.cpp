#include "BCoefficient.h"
#include <cmath>

using namespace Concentration;

BCoefficient::BCoefficient(int size, double deltaStep, double deltaTime, const Coefficient &d, const Coefficient &_Z)
    : Coefficient(size), diffusion(d), Z(_Z)
{
    this->deltaStep = deltaStep;
    this->deltaTime = deltaTime;
}

void BCoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size; ++i)
    {
        double zPlus = Z[i];
        double zMinus = Z[i - 1];
        double zPart, zPart1 = 1, zPart2 = 1;
        if (zPlus != 0)
        {
            double expZ = exp(zPlus);

            zPart1 = expZ * zPlus / (expZ - 1);
        }

        if (zMinus != 0)
        {
            double expZ = exp(zMinus);

            zPart2 = zMinus / (expZ - 1);
        }

        zPart = zPart1 + zPart2;

        coefficient[i] = -diffusion[i] / (deltaStep * deltaStep) * zPart - 1 / deltaTime;
    }
}