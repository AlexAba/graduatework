#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Concentration
{
    class CCoefficient : public Coefficient
    {
    private:
        double deltaStep;

        const Coefficient &diffusion;
        const Coefficient &Z;

    public:
        CCoefficient(int size, double deltaStep, const Coefficient &diffusion, const Coefficient &Z);
        virtual void CalculateCoefficients() override;
    };
} // namespace Concentration