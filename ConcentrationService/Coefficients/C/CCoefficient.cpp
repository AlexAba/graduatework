#include "CCoefficient.h"
#include <cmath>

#include <iostream>

using namespace std;

using namespace Concentration;

CCoefficient::CCoefficient(int size, double deltaStep, const Coefficient &d, const Coefficient &_Z)
    : Coefficient(size), diffusion(d), Z(_Z)
{
    this->deltaStep = deltaStep;
}

void CCoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size; ++i)
    {
        double zPart = 1;
        double z = Z[i - 1];
        if (z != 0)
        {
            double expZ = exp(z);

            zPart = expZ * z / (expZ - 1);
        }
        
        coefficient[i] = -diffusion[i - 1] / (deltaStep * deltaStep) * zPart;
    }
}