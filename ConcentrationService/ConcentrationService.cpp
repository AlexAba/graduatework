#include "ConcentrationService.h"

ConcentrationService::ConcentrationService(int size, Coefficient &a, Coefficient &b, double lastConcentration)
: Coefficient(size), alpha(a), beta(b)
{
    //  When we talk about last concentration we imply one before last array item.
    coefficient[size - 2] = lastConcentration;
}

void ConcentrationService::CalculateCoefficients()
{
    int size = coefficient.size();

    //  We don't calculate first and last item. 
    //  Maybe I'll remove this useless neighbours.
    //  concentration[size - 2] is initialised by user.
    for(int i = size - 3; i > 0; --i)
    {
        //  The formula for concentration is:
        //  n_i = aplha_i+1 * n_i+1 + beta_i+1,
        //  but we don't use alpha_0&beta_0 and alpha_1&beta_1,
        //  so we can shift, in our case, the one item to the left.
        coefficient[i] = alpha[i] * coefficient[i + 1] + beta[i];
    }
}