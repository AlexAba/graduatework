#include "../Coefficients/Coefficient.h"

class ElectronMobilityCoefficient : public Coefficient
{
public:
    ElectronMobilityCoefficient(int size, double val);
    virtual void CalculateCoefficients() override;
};