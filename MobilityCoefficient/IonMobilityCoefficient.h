#include "../Coefficients/Coefficient.h"

class IonMobilityCoefficient : public Coefficient
{
public:
    IonMobilityCoefficient(int size, double val);
    virtual void CalculateCoefficients() override;
};