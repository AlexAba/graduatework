#pragma once

#include "../Coefficient.h"
#include <vector>

using namespace std;

class BaseAlphaCoefficient : public Coefficient
{

protected:
    const Coefficient &A;
    const Coefficient &B;
    const Coefficient &C;

public:
    BaseAlphaCoefficient(int size, double first, const Coefficient &A, const Coefficient &B, const Coefficient &C);
    virtual void CalculateCoefficients() override;
};