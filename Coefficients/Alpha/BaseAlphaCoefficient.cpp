#include "BaseAlphaCoefficient.h"

#include <iostream>
using namespace std;

BaseAlphaCoefficient::BaseAlphaCoefficient(int size, double first, const Coefficient &_A, const Coefficient &_B, const Coefficient &_C)
    : Coefficient(size), A(_A), B(_B), C(_C)
{
    coefficient[1] = first;
}

void BaseAlphaCoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size - 1; ++i)
    {
        coefficient[i + 1] = A[i] / (B[i] - coefficient[i] * C[i]);
    }
}