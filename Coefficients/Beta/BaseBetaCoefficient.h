#pragma once

#include "../Coefficient.h"
#include "../Alpha/BaseAlphaCoefficient.h"
#include <vector>

using namespace std;

class BaseBetaCoefficient : public Coefficient
{
protected:
    const Coefficient &alpha;
    const Coefficient &B;
    const Coefficient &C;
    const Coefficient &D;

public:
    BaseBetaCoefficient(int size, double first,
                        const BaseAlphaCoefficient &alpha, const Coefficient &B, const Coefficient &C, const Coefficient &D);
    virtual void CalculateCoefficients() override;
};