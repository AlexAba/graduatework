#include <iostream>

using namespace std;

#include "BaseBetaCoefficient.h"

BaseBetaCoefficient::BaseBetaCoefficient(int size, double first,
                                         const BaseAlphaCoefficient &_alpha,
                                         const Coefficient &_B, const Coefficient &_C, const Coefficient &_D)
    : Coefficient(size), alpha(_alpha), B(_B), C(_C), D(_D)
{
    coefficient[1] = first;
}

void BaseBetaCoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size - 1; ++i)
    {
        coefficient[i + 1] = (C[i] * coefficient[i] - D[i]) / (B[i] - alpha[i] * C[i]);
    }
}