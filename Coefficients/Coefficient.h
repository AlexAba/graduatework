#pragma once

#include <vector>

using namespace std;

class Coefficient
{
protected:
    vector<double> coefficient;

public:
    Coefficient(int size);
    Coefficient(int size, double val);
    virtual void CalculateCoefficients();
    double operator[](int index) const;
    unsigned int GetSize() const;
    vector<double> GetVectorCopy() const;
};