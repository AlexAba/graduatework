#include "Coefficient.h"

Coefficient::Coefficient(int size)
{
    coefficient = vector<double>(size, 0.);
}

void Coefficient::CalculateCoefficients()
{

}

Coefficient::Coefficient(int size, double val)
{
    coefficient = vector<double>(size, val);
}

double Coefficient::operator[](int index) const
{
    return coefficient[index];
}

unsigned int Coefficient::GetSize() const
{
    return coefficient.size();
}

vector<double> Coefficient::GetVectorCopy() const
{
    return coefficient;
}