#include "../Coefficients/Coefficient.h"

class ElectronDiffusionCoefficient : public Coefficient
{
public:
    ElectronDiffusionCoefficient(int size, double val);
    virtual void CalculateCoefficients() override;
};