#include "../Coefficients/Coefficient.h"

class IonDiffusionCoefficient : public Coefficient
{
public:
    IonDiffusionCoefficient(int size, double val);
    virtual void CalculateCoefficients() override;
};