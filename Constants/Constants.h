#pragma once

namespace PhysConstants
{
    double epsilon0 = 8.85e-12;
    double k = 1.38e-23;
    namespace Electron
    {
        double m = 9.1e-31;
        double q = 1.6e-19;
    }
}