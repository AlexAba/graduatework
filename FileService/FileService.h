#pragma once

#include <string>
#include <vector>

using namespace std;

class FileService
{
public:
    static void WriteData(string fileName, vector<double> x, vector<vector<double>> y);
};