#include "FileService.h"

#include <fstream>

void FileService::WriteData(string fileName, vector<double> x, vector<vector<double>> y)
{
    ofstream out;
    out.open(fileName);
    if (out.is_open())
    {
        for (double i : x)
        {
            out << i << "\t";
        }

        out << endl;

        for (auto cy : y)
        {
            for (double i : cy)
            {
                out << i << "\t";
            }
            out << endl;
        }
    }

    out.close();
}