#include "PotentialService.h"

#include <iostream>
using namespace std;

PotentialService::PotentialService(unsigned int size,
                                   const Coefficient &_alpha, const Coefficient &_beta,
                                   double firstPotential, double lastPotential)
    : Coefficient(size), alpha(_alpha), beta(_beta)
{
    coefficient[0] = firstPotential;
    coefficient[size - 1] = lastPotential;
}

void PotentialService::CalculateCoefficients()
{
    int size = coefficient.size();
    for (int j = size - 2; j > 0; --j)
    {
        coefficient[j] = alpha[j + 1] * coefficient[j + 1] + beta[j + 1];
    }
}