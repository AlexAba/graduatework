#include "CCoefficient.h"

using namespace Potential;

CCoefficient::CCoefficient(int size, double deltaStep) : Coefficient(size)
{
    this->deltaStep = deltaStep;
}

void CCoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size - 1; ++i)
    {
        if (i == 1)
        {
            coefficient[i] = 8 / (3 * deltaStep * deltaStep);
        }
        else if (i == size - 2)
        {
            coefficient[i] = 4 / (3 * deltaStep * deltaStep);
        }
        else
        {
            coefficient[i] = 1 / (deltaStep * deltaStep);
        }
    }
}