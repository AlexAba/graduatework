#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Potential
{
    class CCoefficient : public Coefficient
    {
    private:
        double deltaStep;

    public:
        CCoefficient(int size, double deltaStep);
        virtual void CalculateCoefficients() override;
    };
} // namespace Potential