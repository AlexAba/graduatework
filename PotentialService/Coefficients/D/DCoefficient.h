#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Potential
{
    class DCoefficient : public Coefficient
    {
    private:
        const Coefficient& electronConcentration;
        const Coefficient& ionConcentration;

    public:
        DCoefficient(int size, const Coefficient& electronConcentration, const Coefficient& ionConcentration);
        virtual void CalculateCoefficients() override;
    };
} // namespace Potential
