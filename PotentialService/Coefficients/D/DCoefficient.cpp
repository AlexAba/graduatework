#include "DCoefficient.h"
#include "../../../Constants/Constants.h"

using namespace Potential;
using namespace PhysConstants;

DCoefficient::DCoefficient(int size, const Coefficient &ne, const Coefficient &ni)
    : Coefficient(size), electronConcentration(ne), ionConcentration(ni)
{
}

void DCoefficient::CalculateCoefficients()
{
    int size = GetSize();

    for (int i = 0; i < size; ++i)
    {
        coefficient[i] = -1 / epsilon0 * Electron::q * (ionConcentration[i] - electronConcentration[i]);
    }
}