#include "ACoefficient.h"

using namespace Potential;

ACoefficient::ACoefficient(int size, double deltaStep) : Coefficient(size)
{
    this->deltaStep = deltaStep;
}

void ACoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size - 1; ++i)
    {
        if (i == 1)
        {
            coefficient[i] = 4 / (3 * deltaStep * deltaStep);
        }
        else if (i == size - 2)
        {
            coefficient[i] = 8 / (3 * deltaStep * deltaStep);
        }
        else
        {
            coefficient[i] = 1 / (deltaStep * deltaStep);
        }
    }
}