#pragma once

#include "../../../Coefficients/Coefficient.h"
namespace Potential
{
    class ACoefficient : public Coefficient
    {
    private:
        double deltaStep;

    public:
        ACoefficient(int size, double deltaStep);
        virtual void CalculateCoefficients() override;
    };
} 