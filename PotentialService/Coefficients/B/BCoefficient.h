#pragma once

#include "../../../Coefficients/Coefficient.h"

namespace Potential
{
    class BCoefficient : public Coefficient
    {
    private:
        double deltaStep;

    public:
        BCoefficient(int size, double deltaStep);
        virtual void CalculateCoefficients() override;
    };
} // namespace Potential