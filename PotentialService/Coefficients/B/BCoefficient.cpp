#include "BCoefficient.h"

using namespace Potential;

BCoefficient::BCoefficient(int size, double deltaStep) : Coefficient(size)
{
    this->deltaStep = deltaStep;
}

void BCoefficient::CalculateCoefficients()
{
    int size = coefficient.size();

    for (int i = 1; i < size - 1; ++i)
    {
        if (i == 1)
        {
            coefficient[i] = 4 / (deltaStep * deltaStep);
        }
        else if (i == size - 2)
        {
            coefficient[i] = 4 / (deltaStep * deltaStep);
        }
        else
        {
            coefficient[i] = 2 / (deltaStep * deltaStep);
        }
    }
}