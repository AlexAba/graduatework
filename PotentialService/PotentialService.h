#pragma once

#include <vector>

#include "../Coefficients/Coefficient.h"

using namespace std;

class PotentialService : public Coefficient
{
private:
    const Coefficient &alpha;
    const Coefficient &beta;

    vector<double> potential;

public:
    PotentialService(unsigned int size,
                     const Coefficient &_alpha, const Coefficient &_beta,
                     double firstPotential, double lastPotential);

    virtual void CalculateCoefficients() override;
    vector<double> GetPotential();
};