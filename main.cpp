#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <cmath>

#include "FileService/FileService.h"
#include "PotentialService/PotentialService.h"

#include "Coefficients/Coefficient.h"
#include "Coefficients/Alpha/BaseAlphaCoefficient.h"
#include "Coefficients/Beta/BaseBetaCoefficient.h"
#include "PotentialService/Coefficients/A/ACoefficient.h"
#include "PotentialService/Coefficients/B/BCoefficient.h"
#include "PotentialService/Coefficients/C/CCoefficient.h"
#include "PotentialService/Coefficients/D/DCoefficient.h"

#include "ConcentrationService/ConcentrationService.h"
#include "ConcentrationService/Coefficients/A/ACoefficient.h"
#include "ConcentrationService/Coefficients/B/BCoefficient.h"
#include "ConcentrationService/Coefficients/C/CCoefficient.h"
#include "ConcentrationService/Coefficients/D/DCoefficient.h"
#include "ConcentrationService/Coefficients/Z/ZCoefficient.h"

#include "DiffusionCoefficient/ElectronDiffusionCoefficient.h"
#include "DiffusionCoefficient/IonDiffusionCoefficient.h"
#include "MobilityCoefficient/ElectronMobilityCoefficient.h"
#include "MobilityCoefficient/IonMobilityCoefficient.h"

using namespace std;

using PACoefficient = Potential::ACoefficient;
using PBCoefficient = Potential::BCoefficient;
using PCCoefficient = Potential::CCoefficient;
using PDCoefficient = Potential::DCoefficient;

using CACoefficient = Concentration::ACoefficient;
using CBCoefficient = Concentration::BCoefficient;
using CCCoefficient = Concentration::CCoefficient;
using CDCoefficient = Concentration::DCoefficient;
using CZCoefficient = Concentration::ZCoefficient;

void PrepareLengthForPrint(vector<double> &outVector, double L, unsigned int N);

PotentialService *GetPotential(double L, unsigned int N, const Coefficient &elConcentration, const Coefficient &ionConcentration);

ConcentrationService *GetConcentration(int type, double L, unsigned int N, double deltaTime,
                                       const Coefficient &potential,
                                       const Coefficient &diffusion, const Coefficient &mobility,
                                       const Coefficient &prevConcentration);

double GetLastConcentration(int type, double H, double DN, double DNMinus, double ZNMinus,
                            double deltaTime, double MobN, double fiN, double fiNPlus,
                            double nN, double alphaN, double betaN);
double GetFirstAplha(int type, double H, double D1, double D2, double Z1, double deltaTime, double mob, double fi0, double fi1);
double GetFirstBeta(int type, double H, double D1, double Z1, double deltaTime, double mob, double fi0, double fi1);

void ParseParams(int argc, char **argv,
                 double &L, unsigned int &N, double &dt,
                 double &De, double &Di, double &Mue, double &Mui,
                 double na, unsigned int &repNum, double &pressure);

int main(int argc, char **argv)
{
    string folderName = "Results\\";
    string potentialName = folderName + "Potentials";
    string elConcentrationName = folderName + "ElectronConcentrations";
    string ionConcentrationName = folderName + "IonConcentrations";
    string extention = ".txt";

    double L = 0.05;
    unsigned int N = 1000;
    unsigned int size = N + 2;
    double deltaTime = 1e-12;

    double De = 30;
    double Di = 0.2;
    double Mue = 30;
    double Mui = 1;

    double pressure = 1;

    double initialConcentration = 1e16;
    unsigned int repNum = 100;

    ParseParams(argc, argv, L, N, deltaTime, De, Di, Mue, Mui, initialConcentration, repNum, pressure);

    vector<double> length;
    PrepareLengthForPrint(length, L, N);

    ElectronDiffusionCoefficient electronDiffusion(size, De / pressure);
    IonDiffusionCoefficient ionDiffusion(size, Di / pressure);
    electronDiffusion.CalculateCoefficients();
    ionDiffusion.CalculateCoefficients();

    ElectronMobilityCoefficient electronMobility(size, Mue / pressure);
    IonMobilityCoefficient ionMobility(size, Mui / pressure);
    electronMobility.CalculateCoefficients();
    ionMobility.CalculateCoefficients();

    Coefficient elConcentration(size, initialConcentration);
    Coefficient ionConcentration(size, initialConcentration);

    vector<vector<double>> potentialResult;
    vector<vector<double>> elConcentrationResult;
    vector<vector<double>> ionConcentrationResult;

    for (int i = 0; i < repNum; ++i)
    {
        PotentialService *PS = GetPotential(L, N, elConcentration, ionConcentration);
        ConcentrationService *elCS = GetConcentration(-1, L, N, deltaTime, *PS, electronDiffusion, electronMobility, elConcentration);
        ConcentrationService *ionCS = GetConcentration(1, L, N, deltaTime, *PS, ionDiffusion, ionMobility, ionConcentration);

        potentialResult.push_back(PS->GetVectorCopy());
        elConcentrationResult.push_back(elCS->GetVectorCopy());
        ionConcentrationResult.push_back(ionCS->GetVectorCopy());

        elConcentration = *elCS;
        ionConcentration = *ionCS;

        delete PS;
        delete elCS;
        delete ionCS;
    }

    FileService::WriteData(potentialName + extention, length, potentialResult);
    FileService::WriteData(elConcentrationName + extention, length, elConcentrationResult);
    FileService::WriteData(ionConcentrationName + extention, length, ionConcentrationResult);

    return 0;
}

void PrepareLengthForPrint(vector<double> &outVector, double L, unsigned int N)
{
    double step = L / N;

    outVector = vector<double>(N + 2);

    for (int i = 0; i < N + 2; ++i)
    {
        outVector[i] = step * i;
    }
}

PotentialService *GetPotential(double L, unsigned int N, const Coefficient &elConcentration, const Coefficient &ionConcentration)
{
    double H = L / N;
    int size = N + 2;

    double firstAlpha = 0, firstBeta = -300;
    double firstPotential = -300, lastPotential = 0;

    PACoefficient A(size, H);
    PBCoefficient B(size, H);
    PCCoefficient C(size, H);
    PDCoefficient D(size, elConcentration, ionConcentration);

    A.CalculateCoefficients();
    B.CalculateCoefficients();
    C.CalculateCoefficients();
    D.CalculateCoefficients();

    BaseAlphaCoefficient alpha(size, firstAlpha, A, B, C);
    alpha.CalculateCoefficients();
    BaseBetaCoefficient beta(size, firstBeta, alpha, B, C, D);
    beta.CalculateCoefficients();

    PotentialService *PS = new PotentialService(size, alpha, beta, firstPotential, lastPotential);
    PS->CalculateCoefficients();
    return PS;
}

ConcentrationService *GetConcentration(int type, double L, unsigned int N, double deltaTime,
                                       const Coefficient &potential,
                                       const Coefficient &diffusion, const Coefficient &mobility,
                                       const Coefficient &prevConcentration)
{
    double H = L / N;
    int size = N + 2;

    CZCoefficient Z(size, type, potential, diffusion, mobility);
    Z.CalculateCoefficients();

    CACoefficient A(size, H, diffusion, Z);
    CBCoefficient B(size, H, deltaTime, diffusion, Z);
    CCCoefficient C(size, H, diffusion, Z);
    CDCoefficient D(size, deltaTime, prevConcentration);

    A.CalculateCoefficients();
    B.CalculateCoefficients();
    C.CalculateCoefficients();
    D.CalculateCoefficients();

    double firstAlpha = GetFirstAplha(type, H, diffusion[1], diffusion[2], Z[1], deltaTime, mobility[1], potential[0], potential[1]);
    double firstBeta = GetFirstBeta(type, H, diffusion[1], Z[1], deltaTime, mobility[1], potential[0], potential[1]);

    BaseAlphaCoefficient alpha(size - 1, firstAlpha, A, B, C);
    alpha.CalculateCoefficients();
    BaseBetaCoefficient beta(size - 1, firstBeta, alpha, B, C, D);
    beta.CalculateCoefficients();

    double lastConcentration = GetLastConcentration(type, H, diffusion[size - 2], diffusion[size - 3], Z[size - 3],
                                                    deltaTime, mobility[size - 1], potential[size - 2], potential[size - 1],
                                                    prevConcentration[size - 2], alpha[size - 2], beta[size - 2]);

    ConcentrationService *CS = new ConcentrationService(size, alpha, beta, lastConcentration);
    CS->CalculateCoefficients();

    return CS;
}

double GetFirstAplha(int type, double H, double D1, double D2, double Z1, double deltaTime, double mob, double fi0, double fi1)
{
    double expZ = 1;
    double expZMinusOne = 1;
    if (Z1 != 0)
    {
        expZ = exp(Z1);
        expZMinusOne = expZ - 1;
    }
    else
    {
        Z1 = 1;
    }

    double numerator, denominator;

    double potentialDiff = fi0 - fi1;
    if (potentialDiff * type > 0)
    {
        potentialDiff = 0;
    }

    numerator = D2 * Z1 * deltaTime;
    denominator = (H * H) * expZMinusOne + D1 * expZ * Z1 * deltaTime - type * 2 * mob * potentialDiff * expZMinusOne * deltaTime;

    return numerator / denominator;
}

double GetFirstBeta(int type, double H, double D1, double Z1, double deltaTime, double mob, double fi0, double fi1)
{
    double expZ = 1;
    double expZMinusOne = 1;
    if (Z1 != 0)
    {
        expZ = exp(Z1);
        expZMinusOne = expZ - 1;
    }
    else
    {
        Z1 = 1;
    }

    double potentialDiff = fi0 - fi1;
    if (potentialDiff * type > 0)
    {
        potentialDiff = 0;
    }

    double numerator, denominator;

    numerator = (H * H) * expZMinusOne;
    denominator = (H * H) * expZMinusOne + D1 * expZ * Z1 * deltaTime - type * 2 * mob * potentialDiff * deltaTime * expZMinusOne;

    return numerator / denominator;
}

double GetLastConcentration(int type, double H, double DN, double DNMinus, double ZNMinus,
                            double deltaTime, double MobN, double fiN, double fiNPlus,
                            double nN, double alphaN, double betaN)
{
    double expZ = 1;
    double expZMinusOne = 1;
    if (ZNMinus != 0)
    {
        expZ = exp(ZNMinus);
        expZMinusOne = expZ - 1;
    }
    else
    {
        ZNMinus = 1;
    }

    double potentialDiff = fiNPlus - fiN;
    if (potentialDiff * type < 0)
    {
        potentialDiff = 0;
    }

    double numerator, denominator;

    numerator = DNMinus * expZ * ZNMinus * nN * (H * H) * expZMinusOne + betaN * deltaTime;

    denominator = DN * expZ * ((H * H) * expZMinusOne - type * MobN * potentialDiff * expZMinusOne + DN * ZNMinus * deltaTime) - alphaN * deltaTime;

    return numerator / denominator;
}

void ParseParams(int argc, char **argv,
                 double &L, unsigned int &N, double &dt,
                 double &De, double &Di, double &Mue, double &Mui,
                 double initialConcentration, unsigned int &repNum, double &pressure)
{
    for (int i = 1; i < argc; ++i)
    {
        if (!strcmp(argv[i], "-L"))
        {
            L = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-N"))
        {
            N = atoi(argv[++i]);
        }
        else if (!strcmp(argv[i], "-dt"))
        {
            dt = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-De"))
        {
            De = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-Di"))
        {
            Di = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-Mue"))
        {
            Mue = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-Mui"))
        {
            Mui = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-initConcentraion"))
        {
            initialConcentration = atof(argv[++i]);
        }
        else if (!strcmp(argv[i], "-repNum"))
        {
            repNum = atoi(argv[++i]);
        }
        else if (!strcmp(argv[i], "-p"))
        {
            pressure = atof(argv[++i]);
        }
    }
}